import { Row, Button, Container } from 'react-bootstrap';
import { useState} from 'react';
import './calculadora.css';


function Calculadora() {

    //hacer CONST PARA DISPLAY, OPERACIONES Y NUMEROS 
   // const [display,setDisplay] = useState(display);
    const [numero,setNumero] = useState('');
    //const [display,setDisplay] = useState('');
    




    return (


        <div className="box">
        <Container className="p-4 mt-4">
            <Row>
                <div className="col-12">
               {/* // añadir {display} */}
                    <input dir="rtl" type="text" class="form-control lg" value={numero} onInput={(e) => setNumero(e.target.value)}></input> 
                </div>
            </Row>
            <Row>
                <div className="col-3"><Button onClick={()=>setNumero(1)} value={numero} variant="outline-secondary" size="lg">1</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(2)}  variant="outline-secondary" size="lg">2</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(3)} variant="outline-secondary" size="lg">3</Button></div>
                <div className="col-3"><Button  variant="outline-secondary" size="lg">*</Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button onClick={()=>setNumero(4)} value={numero} variant="outline-secondary" size="lg">4</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(5)} value={numero} variant="outline-secondary" size="lg">5</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(6)} value={numero} variant="outline-secondary" size="lg">6</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">/</Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button onClick={()=>setNumero(7)} value={numero} variant="outline-secondary" size="lg">7</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(8)} value={numero} variant="outline-secondary" size="lg">8</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero(9)} value={numero} variant="outline-secondary" size="lg">9</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">-</Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button onClick={()=>setNumero(0)} value={numero} variant="outline-secondary" size="lg">0</Button></div>
                <div className="col-3"><Button onClick={()=>setNumero('')} value={numero}variant="outline-secondary" size="lg">C</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">=</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">+</Button></div>
            </Row>
        </Container>
        </div>
    )
}

export default Calculadora